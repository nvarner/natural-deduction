use crate::proof::coords::Coords;

#[derive(Eq, PartialEq, Debug, Clone)]
pub enum LawCitation {
    Assumption,
    Arbitrary,
    Premise,
    AndElim {
        and: Coords,
    },
    AndIntro {
        a: Coords,
        b: Coords,
    },
    OrElim {
        or: Coords,
        a_assumption: Coords,
        b_assumption: Coords,
    },
    OrIntro {
        side: Coords,
    },
    ImpliesElim {
        implication: Coords,
        premise: Coords,
    },
    ImpliesIntro {
        assumption: Coords,
    },
    IffElim {
        iff: Coords,
        premise: Coords,
    },
    IffIntro {
        assumption_a: Coords,
        assumption_b: Coords,
    },
    DoubleNegationElim {
        double_negation: Coords,
    },
    NegationElim {
        p: Coords,
        not_p: Coords,
    },
    NegationIntro {
        assumption: Coords,
    },
    FalseElim {
        f: Coords,
    },
    ExistsIntro {
        specified: Coords,
    },
    ExistsElim {
        exists: Coords,
    },
    ForAllIntro {
        assumption: Coords,
    },
    ForAllElim {
        for_all: Coords,
    },
}

use LawCitation::*;

pub fn assumption() -> LawCitation {
    Assumption
}

pub fn arbitrary() -> LawCitation {
    Arbitrary
}

pub fn premise() -> LawCitation {
    Premise
}

pub fn and_elim(and: Coords) -> LawCitation {
    AndElim { and }
}

pub fn and_intro(left: Coords, right: Coords) -> LawCitation {
    AndIntro { a: left, b: right }
}

pub fn or_elim(
    or: Coords,
    a_assumption: Coords,
    b_assumption: Coords,
) -> LawCitation {
    OrElim {
        or,
        a_assumption,
        b_assumption,
    }
}

pub fn or_intro(side: Coords) -> LawCitation {
    OrIntro { side }
}

pub fn implies_elim(implication: Coords, premise: Coords) -> LawCitation {
    ImpliesElim {
        implication,
        premise,
    }
}

pub fn implies_intro(assumption: Coords) -> LawCitation {
    ImpliesIntro { assumption }
}

pub fn iff_elim(iff: Coords, premise: Coords) -> LawCitation {
    IffElim { iff, premise }
}

pub fn iff_intro(assumption_a: Coords, assumption_b: Coords) -> LawCitation {
    IffIntro {
        assumption_a,
        assumption_b,
    }
}

pub fn double_negation_elim(double_negation: Coords) -> LawCitation {
    DoubleNegationElim { double_negation }
}

pub fn negation_elim(p: Coords, not_p: Coords) -> LawCitation {
    NegationElim { p, not_p }
}

pub fn negation_intro(assumption: Coords) -> LawCitation {
    NegationIntro { assumption }
}

pub fn false_elim(f: Coords) -> LawCitation {
    FalseElim { f }
}

pub fn exists_intro(specified: Coords) -> LawCitation {
    ExistsIntro { specified }
}

pub fn exists_elim(exists: Coords) -> LawCitation {
    ExistsElim { exists }
}

pub fn for_all_intro(assumption: Coords) -> LawCitation {
    ForAllIntro { assumption }
}

pub fn for_all_elim(for_all: Coords) -> LawCitation {
    ForAllElim { for_all }
}
