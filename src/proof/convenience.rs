macro_rules! pf_body_helper {
    ( { $($prev:expr)* }; ) => (vec![$($prev),*]);
    ( { $($prev:expr)* }; { $($box_insides:tt)* } $($rest:tt)* ) => {
        pf_body_helper!(
            { $($prev)* pf_box!($($box_insides)*) };
            $($rest)*
        )
    };
    ( { $($prev:expr)* }; ($prop:expr, $citation:expr) $($rest:tt)* ) => {
        pf_body_helper!(
            { $($prev)* line($prop, $citation) };
            $($rest)*
        )
    }
}

/// Returns a vec containing all things passed in
macro_rules! pf_body {
    ($($all:tt)*) =>
        (pf_body_helper!({}; $($all)*));
}

/// Constructs a proof with the given premises and proof structures.
#[macro_export]
macro_rules! pf {
    {
        [$($premise:expr),* $(,)?];
        $($all:tt)*
    } => {
        $crate::proof::Proof::new(
            ::velcro::hash_set![$($premise),*],
            pf_body!($($all)*)
        )
    };
}

#[cfg(test)]
mod test {
    use crate::prelude::*;

    #[test]
    fn test_pf_body_line() {
        let actual = pf_body!((atom("p"), assumption()));
        assert_eq!(vec![line(atom("p"), assumption())], actual);
    }

    #[test]
    fn test_pf_body_box() {
        let actual = pf_body!({ (atom("p"), assumption()) });
        assert_eq!(vec![pf_box![(atom("p"), assumption())]], actual);
    }
}
