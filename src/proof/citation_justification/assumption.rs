//! Valid iff the first line in a box

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn assumption(&self, line_coords: &Coords) -> PfResult {
        if line_coords.is_first_line_of_box() {
            Ok(())
        } else {
            line_err!(self, AssumptionNotFirstLineOfBox, line_coords)
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_assumption() {
        let proof = pf! {
            [];
            {
                (atom("p"), assumption())
            }
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_assumption_not_box() {
        let proof = pf! {
            [];
            (atom("p"), assumption())
        };
        assert_eq!(
            line_err!(proof, AssumptionNotFirstLineOfBox, &coords![0]),
            proof.is_justified()
        );
    }

    #[test]
    fn test_assumption_not_first() {
        let proof = pf! {
            [];
            {
                (atom("q"), assumption())
                (atom("p"), assumption())
            }
        };
        assert_eq!(
            line_err!(proof, AssumptionNotFirstLineOfBox, &coords![0, 1]),
            proof.is_justified()
        );
    }
}
