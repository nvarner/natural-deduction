//! Valid iff left and right side of and statement are matched

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn and_intro(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        a_coords: &Coords,
        b_coords: &Coords,
    ) -> PfResult {
        let a_prop = self.get_citation_prop(line_coords, a_coords)?;
        let b_prop = self.get_citation_prop(line_coords, b_coords)?;
        match prop {
            Binary(left, And, right)
                if (**left == *a_prop && **right == *b_prop)
                    || (**left == *b_prop && **right == *a_prop) =>
            {
                Ok(())
            }
            Binary(left, And, _) if **left == *a_prop || **left == *b_prop => {
                cite2_err!(
                    self,
                    AndIntroNotMatchRight,
                    line_coords,
                    a_coords,
                    b_coords
                )
            }
            Binary(_, And, right)
                if **right == *a_prop || **right == *b_prop =>
            {
                cite2_err!(
                    self,
                    AndIntroNotMatchLeft,
                    line_coords,
                    a_coords,
                    b_coords
                )
            }
            Binary(_, And, _) => {
                cite2_err!(
                    self,
                    AndIntroNotMatchAny,
                    line_coords,
                    a_coords,
                    b_coords
                )
            }
            _ => line_err!(self, AndIntroNotAnd, line_coords),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_and_intro() {
        let proof = pf! {
            [atom("p"), atom("q")];
            (atom("p"), premise())
            (atom("q"), premise())
            (binary(atom("p"), And, atom("q")), and_intro(coords![0], coords![1]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_and_intro_no_right() {
        let proof = pf! {
            [atom("p"), atom("q")];
            (atom("p"), premise())
            (atom("q"), premise())
            (binary(atom("p"), And, atom("q")), and_intro(coords![0], coords![0]))
        };
        assert_eq!(
            cite2_err!(
                proof,
                AndIntroNotMatchRight,
                &coords![2],
                &coords![0],
                &coords![0]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_and_intro_no_left() {
        let proof = pf! {
            [atom("p"), atom("q")];
            (atom("p"), premise())
            (atom("q"), premise())
            (binary(atom("p"), And, atom("q")), and_intro(coords![1], coords![1]))
        };
        assert_eq!(
            cite2_err!(
                proof,
                AndIntroNotMatchLeft,
                &coords![2],
                &coords![1],
                &coords![1]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_and_intro_none() {
        let proof = pf! {
            [atom("p"), atom("q")];
            (atom("p"), premise())
            (atom("q"), premise())
            (binary(atom("a"), And, atom("b")), and_intro(coords![0], coords![1]))
        };
        assert_eq!(
            cite2_err!(
                proof,
                AndIntroNotMatchAny,
                &coords![2],
                &coords![0],
                &coords![1]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_and_intro_not_and() {
        let proof = pf! {
            [atom("p"), atom("q")];
            (atom("p"), premise())
            (atom("q"), premise())
            (binary(atom("p"), Or, atom("q")), and_intro(coords![0], coords![1]))
        };
        assert_eq!(
            line_err!(proof, AndIntroNotAnd, &coords![2]),
            proof.is_justified()
        );
    }
}
