//! Valid iff citation matches the left or right side

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn or_intro(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        side_coords: &Coords,
    ) -> PfResult {
        let side_prop = self.get_citation_prop(line_coords, side_coords)?;
        match prop {
            Binary(p, Or, q) if **p == *side_prop || **q == *side_prop => {
                Ok(())
            }
            Binary(_, Or, _) => {
                cite_err!(
                    self,
                    OrIntroDoesNotMatchEitherSide,
                    line_coords,
                    side_coords
                )
            }
            _ => {
                line_err!(self, OrIntroNotOr, line_coords)
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_or_intro_left() {
        let proof = pf! {
            [atom("p")];
            (atom("p"), premise())
            (binary(atom("p"), Or, atom("q")), or_intro(coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_or_intro_right() {
        let proof = pf! {
            [atom("p")];
            (atom("p"), premise())
            (binary(atom("q"), Or, atom("p")), or_intro(coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_or_intro_neither_side() {
        let proof = pf! {
            [atom("p")];
            (atom("p"), premise())
            (binary(atom("q"), Or, atom("r")), or_intro(coords![0]))
        };
        assert_eq!(
            cite_err!(
                proof,
                OrIntroDoesNotMatchEitherSide,
                &coords![1],
                &coords![0]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_or_intro_not_or() {
        let proof = pf! {
            [atom("p")];
            (atom("p"), premise())
            (atom("p"), or_intro(coords![0]))
        };
        assert_eq!(
            line_err!(proof, OrIntroNotOr, &coords![1]),
            proof.is_justified()
        )
    }
}
