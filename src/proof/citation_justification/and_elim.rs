//! Valid iff matches the left or right side of and statement

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn and_elim(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        and_coords: &Coords,
    ) -> PfResult {
        let and_prop = self.get_citation_prop(line_coords, and_coords)?;
        match and_prop {
            Binary(p, And, q) if **p == *prop || **q == *prop => Ok(()),
            Binary(_, And, _) => {
                cite_err!(
                    self,
                    AndElimDoesNotMatchEitherSide,
                    line_coords,
                    and_coords
                )
            }
            _ => {
                cite_err!(self, AndElimCitationNotAnd, line_coords, and_coords)
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_and_elim_left() {
        let proof = pf! {
            [binary(atom("p"), And, atom("q"))];
            (binary(atom("p"), And, atom("q")), premise())
            (atom("p"), and_elim(coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_and_elim_right() {
        let proof = pf! {
            [binary(atom("p"), And, atom("q"))];
            (binary(atom("p"), And, atom("q")), premise())
            (atom("q"), and_elim(coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_and_elim_neither_side() {
        let proof = pf! {
            [binary(atom("p"), And, atom("q"))];
            (binary(atom("p"), And, atom("q")), premise())
            (atom("r"), and_elim(coords![0]))
        };
        assert_eq!(
            cite_err!(
                proof,
                AndElimDoesNotMatchEitherSide,
                &coords![1],
                &coords![0]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_and_elim_citation_not_and() {
        let proof = pf! {
            [binary(atom("p"), Or, atom("q"))];
            (binary(atom("p"), Or, atom("q")), premise())
            (atom("p"), and_elim(coords![0]))
        };
        assert_eq!(
            cite_err!(proof, AndElimCitationNotAnd, &coords![1], &coords![0]),
            proof.is_justified()
        );
    }
}
