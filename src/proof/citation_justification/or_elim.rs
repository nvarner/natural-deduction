//! Valid iff top of assumptions match or, conclusions match each other and
//! statement

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn or_elim(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        or_coords: &Coords,
        a_assumption_coords: &Coords,
        b_assumption_coords: &Coords,
    ) -> PfResult {
        let (left, right) = match self
            .get_citation_prop(line_coords, or_coords)?
        {
            Binary(left, Or, right) => Ok((left, right)),
            _ => cite_err!(self, OrElimCitationNotOr, line_coords, or_coords),
        }?;
        let a_box = self.get_citation_box(line_coords, a_assumption_coords)?;
        let b_box = self.get_citation_box(line_coords, b_assumption_coords)?;

        let left_a_and_right_b =
            **left == a_box.first().prop && **right == b_box.first().prop;
        let left_b_and_right_a =
            **left == b_box.first().prop && **right == a_box.first().prop;
        let a_conclusion = *prop == a_box.last().prop;
        let b_conclusion = *prop == b_box.last().prop;

        if !left_a_and_right_b && !left_b_and_right_a {
            cite3_err!(
                self,
                OrElimNotMatch,
                line_coords,
                or_coords,
                a_assumption_coords,
                b_assumption_coords
            )
        } else if !a_conclusion {
            cite_err!(
                self,
                OrElimConclusionNotMatch,
                line_coords,
                a_assumption_coords
            )
        } else if !b_conclusion {
            cite_err!(
                self,
                OrElimConclusionNotMatch,
                line_coords,
                b_assumption_coords
            )
        } else {
            Ok(())
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_or_elim() {
        let proof = pf! {
            [binary(atom("r"), Or, atom("r"))];
            (binary(atom("r"), Or, atom("r")), premise())
            {
                (atom("r"), assumption())
            }
            (atom("r"), or_elim(coords![0], coords![1], coords![1]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_or_elim_not_match() {
        let proof = pf! {
            [binary(atom("r"), Or, atom("r"))];
            (binary(atom("r"), Or, atom("r")), premise())
            {
                (atom("z"), assumption())
            }
            (atom("z"), or_elim(coords![0], coords![1], coords![1]))
        };
        assert_eq!(
            cite3_err!(
                proof,
                OrElimNotMatch,
                &coords![2],
                &coords![0],
                &coords![1],
                &coords![1]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_or_elim_conclusion_not_match() {
        let proof = pf! {
            [binary(atom("r"), Or, atom("r"))];
            (binary(atom("r"), Or, atom("r")), premise())
            {
                (atom("r"), assumption())
            }
            (atom("p"), or_elim(coords![0], coords![1], coords![1]))
        };
        assert_eq!(
            cite_err!(
                proof,
                OrElimConclusionNotMatch,
                &coords![2],
                &coords![1]
            ),
            proof.is_justified()
        );
    }
}
