//! Valid iff the premise matches one side and the conclusion matches the other

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn iff_elim(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        iff_coords: &Coords,
        premise_coords: &Coords,
    ) -> PfResult {
        let iff_prop = self.get_citation_prop(line_coords, iff_coords)?;
        let premise_prop =
            self.get_citation_prop(line_coords, premise_coords)?;
        match iff_prop {
            Binary(premise, Iff, conclusion)
            | Binary(conclusion, Iff, premise)
                if (**premise == *premise_prop) && (**conclusion == *prop) =>
            {
                Ok(())
            }
            Binary(premise, Iff, _) | Binary(_, Iff, premise)
                if **premise == *premise_prop =>
            {
                cite_err!(
                    self,
                    IffElimConclusionDoesNotMatch,
                    line_coords,
                    iff_coords
                )
            }
            Binary(_, Iff, _) => {
                cite2_err!(
                    self,
                    IffElimPremiseDoesNotMatch,
                    line_coords,
                    iff_coords,
                    premise_coords
                )
            }
            _ => {
                cite_err!(self, IffElimCitationNotIff, line_coords, iff_coords)
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_iff_elim_left_to_right() {
        let proof = pf! {
            [binary(atom("p"), Iff, atom("q")), atom("p")];
            (atom("p"), premise())
            (binary(atom("p"), Iff, atom("q")), premise())
            (atom("q"), iff_elim(coords![1], coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified())
    }

    #[test]
    fn test_iff_elim_right_to_left() {
        let proof = pf! {
            [binary(atom("p"), Iff, atom("q")), atom("q")];
            (atom("q"), premise())
            (binary(atom("p"), Iff, atom("q")), premise())
            (atom("p"), iff_elim(coords![1], coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified())
    }

    #[test]
    fn test_iff_elim_conclusion_does_not_match() {
        let proof = pf! {
            [binary(atom("p"), Iff, atom("q")), atom("p")];
            (atom("p"), premise())
            (binary(atom("p"), Iff, atom("q")), premise())
            (atom("r"), iff_elim(coords![1], coords![0]))
        };
        assert_eq!(
            cite_err!(
                proof,
                IffElimConclusionDoesNotMatch,
                &coords![2],
                &coords![1]
            ),
            proof.is_justified()
        )
    }

    #[test]
    fn test_iff_elim_premise_does_not_match() {
        let proof = pf! {
            [binary(atom("p"), Iff, atom("q")), atom("r")];
            (atom("r"), premise())
            (binary(atom("p"), Iff, atom("q")), premise())
            (atom("q"), iff_elim(coords![1], coords![0]))
        };
        assert_eq!(
            cite2_err!(
                proof,
                IffElimPremiseDoesNotMatch,
                &coords![2],
                &coords![1],
                &coords![0]
            ),
            proof.is_justified()
        )
    }

    #[test]
    fn test_iff_elim_citation_not_implies() {
        let proof = pf! {
            [atom("p")];
            (atom("p"), premise())
            (atom("p"), iff_elim(coords![0], coords![0]))
        };
        assert_eq!(
            cite_err!(proof, IffElimCitationNotIff, &coords![1], &coords![0]),
            proof.is_justified()
        )
    }
}
