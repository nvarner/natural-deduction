//! Valid iff one is the negation of the other

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn negation_elim(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        a_coords: &Coords,
        b_coords: &Coords,
    ) -> PfResult {
        let a_prop = self.get_citation_prop(line_coords, a_coords)?;
        let b_prop = self.get_citation_prop(line_coords, b_coords)?;
        if let F = prop {
            match (a_prop, b_prop) {
                (other_prop, Unary(Not, negated))
                | (Unary(Not, negated), other_prop)
                    if **negated == *other_prop =>
                {
                    Ok(())
                }
                _ => cite2_err!(
                    self,
                    NegationElimNotNegation,
                    line_coords,
                    a_coords,
                    b_coords
                ),
            }
        } else {
            line_err!(self, NegationElimNotFalse, line_coords)
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_negation_elim() {
        let proof = pf! {
            [atom("p"), unary(Not, atom("p"))];
            (atom("p"), premise())
            (unary(Not, atom("p")), premise())
            (f(), negation_elim(coords![0], coords![1]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_negation_elim_double_negation() {
        let proof = pf! {
            [unary(Not, unary(Not, atom("p"))), unary(Not, atom("p"))];
            (unary(Not, unary(Not, atom("p"))), premise())
            (unary(Not, atom("p")), premise())
            (f(), negation_elim(coords![0], coords![1]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_negation_elim_not_negation() {
        let proof = pf! {
            [atom("p"), atom("q")];
            (atom("p"), premise())
            (atom("q"), premise())
            (f(), negation_elim(coords![0], coords![1]))
        };
        assert_eq!(
            cite2_err!(
                proof,
                NegationElimNotNegation,
                &coords![2],
                &coords![0],
                &coords![1]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_negation_elim_not_false() {
        let proof = pf! {
            [atom("p"), unary(Not, atom("p"))];
            (atom("p"), premise())
            (unary(Not, atom("p")), premise())
            (atom("p"), negation_elim(coords![0], coords![1]))
        };
        assert_eq!(
            line_err!(proof, NegationElimNotFalse, &coords![2]),
            proof.is_justified()
        );
    }
}
