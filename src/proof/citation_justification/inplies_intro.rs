//! Valid iff assumption box top matches premise and bottom matches conclusion

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn implies_intro(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        assumption_box_coords: &Coords,
    ) -> PfResult {
        let assumption_box =
            self.get_citation_box(line_coords, assumption_box_coords)?;
        match prop {
            Binary(premise, Implies, conclusion)
                if **premise == assumption_box.first().prop
                    && **conclusion == assumption_box.last().prop =>
            {
                Ok(())
            }
            Binary(_, Implies, conclusion)
                if **conclusion == assumption_box.last().prop =>
            {
                cite_err!(
                    self,
                    ImpliesIntroPremiseDoesNotMatch,
                    line_coords,
                    assumption_box_coords
                )
            }
            Binary(_, Implies, _) => cite_err!(
                self,
                ImpliesIntroConclusionDoesNotMatch,
                line_coords,
                assumption_box_coords
            ),
            _ => line_err!(self, ImpliesIntroNotImplies, line_coords),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_implies_intro() {
        let proof = pf! {
            [];
            {
                (atom("p"), assumption())
            }
            (binary(atom("p"), Implies, atom("p")), implies_intro(coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_implies_intro_premise_does_not_match() {
        let proof = pf! {
            [];
            {
                (atom("p"), assumption())
            }
            (binary(atom("q"), Implies, atom("p")), implies_intro(coords![0]))
        };
        assert_eq!(
            cite_err!(
                proof,
                ImpliesIntroPremiseDoesNotMatch,
                &coords![1],
                &coords![0]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_implies_intro_conclusion_does_not_match() {
        let proof = pf! {
            [];
            {
                (atom("p"), assumption())
            }
            (binary(atom("p"), Implies, atom("q")), implies_intro(coords![0]))
        };
        assert_eq!(
            cite_err!(
                proof,
                ImpliesIntroConclusionDoesNotMatch,
                &coords![1],
                &coords![0]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_implies_intro_not_implies() {
        let proof = pf! {
            [];
            {
                (atom("p"), assumption())
            }
            (atom("p"), implies_intro(coords![0]))
        };
        assert_eq!(
            line_err!(proof, ImpliesIntroNotImplies, &coords![1]),
            proof.is_justified()
        );
    }
}
