//! Valid iff inside of negation matches the top of the assumption box and the bottom is false

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn negation_intro(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        assumption_coords: &Coords,
    ) -> PfResult {
        let assumption =
            self.get_citation_box(line_coords, assumption_coords)?;
        match prop {
            Unary(Not, negated)
                if (**negated == assumption.first().prop)
                    && (assumption.last().prop == F) =>
            {
                Ok(())
            }
            Unary(Not, negated) if **negated == assumption.first().prop => {
                cite_err!(
                    self,
                    NegationIntroNoFalse,
                    line_coords,
                    assumption_coords
                )
            }
            Unary(Not, _) => cite_err!(
                self,
                NegationIntroAssumptionDoesNotMatch,
                line_coords,
                assumption_coords
            ),
            _ => line_err!(self, NegationIntroNotNegation, line_coords),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_negation_intro() {
        let proof = pf! {
            [f()];
            {
                (atom("p"), assumption())
                (f(), premise())
            }
            (unary(Not, atom("p")), negation_intro(coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_negation_intro_no_false() {
        let proof = pf! {
            [f()];
            {
                (atom("p"), assumption())
            }
            (unary(Not, atom("p")), negation_intro(coords![0]))
        };
        assert_eq!(
            cite_err!(proof, NegationIntroNoFalse, &coords![1], &coords![0]),
            proof.is_justified()
        );
    }

    #[test]
    fn test_negation_intro_assumption_does_not_match() {
        let proof = pf! {
            [f()];
            {
                (atom("p"), assumption())
                (f(), premise())
            }
            (unary(Not, atom("q")), negation_intro(coords![0]))
        };
        assert_eq!(
            cite_err!(
                proof,
                NegationIntroAssumptionDoesNotMatch,
                &coords![1],
                &coords![0]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_negation_intro_not_negation() {
        let proof = pf! {
            [f()];
            {
                (atom("p"), assumption())
                (f(), premise())
            }
            (atom("q"), negation_intro(coords![0]))
        };
        assert_eq!(
            line_err!(proof, NegationIntroNotNegation, &coords![1]),
            proof.is_justified()
        );
    }
}
