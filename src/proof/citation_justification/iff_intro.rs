//! Valid iff one assumption box top matches left and bottom matches right, vice versa for the other

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn iff_intro(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        assumption_box_a_coords: &Coords,
        assumption_box_b_coords: &Coords,
    ) -> PfResult {
        let assumption_box_a =
            self.get_citation_box(line_coords, assumption_box_a_coords)?;
        let assumption_box_b =
            self.get_citation_box(line_coords, assumption_box_b_coords)?;
        match prop {
            Binary(a, Iff, b) | Binary(b, Iff, a)
                if (**a == assumption_box_a.first().prop)
                    && (**a == assumption_box_b.last().prop)
                    && (**b == assumption_box_a.last().prop)
                    && (**b == assumption_box_b.first().prop) =>
            {
                Ok(())
            }
            Binary(_, Iff, _) => {
                cite2_err!(
                    self,
                    IffIntroDoesNotMatch,
                    line_coords,
                    assumption_box_a_coords,
                    assumption_box_b_coords
                )
            }
            _ => line_err!(self, IffIntroNotIff, line_coords),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_iff_intro_left_to_right_first() {
        let proof = pf! {
            [atom("p"), atom("q")];
            {
                (atom("p"), assumption())
                (atom("q"), premise())
            }
            {
                (atom("q"), assumption())
                (atom("p"), premise())
            }
            (binary(atom("p"), Iff, atom("q")), iff_intro(coords![0], coords![1]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_iff_intro_right_to_left_first() {
        let proof = pf! {
            [atom("p"), atom("q")];
            {
                (atom("p"), assumption())
                (atom("q"), premise())
            }
            {
                (atom("q"), assumption())
                (atom("p"), premise())
            }
            (binary(atom("q"), Iff, atom("p")), iff_intro(coords![0], coords![1]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_iff_intro_does_not_match() {
        let proof = pf! {
            [atom("p"), atom("q")];
            {
                (atom("p"), assumption())
                (atom("q"), premise())
            }
            {
                (atom("q"), assumption())
                (atom("p"), premise())
            }
            (binary(atom("p"), Iff, atom("p")), iff_intro(coords![0], coords![1]))
        };
        assert_eq!(
            cite2_err!(
                proof,
                IffIntroDoesNotMatch,
                &coords![2],
                &coords![0],
                &coords![1]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_iff_intro_not_iff() {
        let proof = pf! {
            [atom("p"), atom("q")];
            {
                (atom("p"), assumption())
                (atom("q"), premise())
            }
            {
                (atom("q"), assumption())
                (atom("p"), premise())
            }
            (binary(atom("p"), Implies, atom("q")), iff_intro(coords![0], coords![1]))
        };
        assert_eq!(
            line_err!(proof, IffIntroNotIff, &coords![2]),
            proof.is_justified()
        );
    }
}
