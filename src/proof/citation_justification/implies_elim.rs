//! Valid iff the premise matches and the conclusion matches

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn implies_elim(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        implication_coords: &Coords,
        premise_coords: &Coords,
    ) -> PfResult {
        let implication_prop =
            self.get_citation_prop(line_coords, implication_coords)?;
        let premise_prop =
            self.get_citation_prop(line_coords, premise_coords)?;
        match implication_prop {
            Binary(premise, Implies, conclusion)
                if (**premise == *premise_prop) && (**conclusion == *prop) =>
            {
                Ok(())
            }
            Binary(premise, Implies, _) if **premise == *premise_prop => {
                cite_err!(
                    self,
                    ImpliesElimConclusionDoesNotMatch,
                    line_coords,
                    implication_coords
                )
            }
            Binary(_, Implies, _) => {
                cite2_err!(
                    self,
                    ImpliesElimPremiseDoesNotMatch,
                    line_coords,
                    implication_coords,
                    premise_coords
                )
            }
            _ => cite_err!(
                self,
                ImpliesElimCitationNotImplies,
                line_coords,
                implication_coords
            ),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_implies_elim() {
        let proof = pf! {
            [binary(atom("p"), Implies, atom("q")), atom("p")];
            (atom("p"), premise())
            (binary(atom("p"), Implies, atom("q")), premise())
            (atom("q"), implies_elim(coords![1], coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified())
    }

    #[test]
    fn test_implies_elim_conclusion_does_not_match() {
        let proof = pf! {
            [binary(atom("p"), Implies, atom("q")), atom("p")];
            (atom("p"), premise())
            (binary(atom("p"), Implies, atom("q")), premise())
            (atom("r"), implies_elim(coords![1], coords![0]))
        };
        assert_eq!(
            cite_err!(
                proof,
                ImpliesElimConclusionDoesNotMatch,
                &coords![2],
                &coords![1]
            ),
            proof.is_justified()
        )
    }

    #[test]
    fn test_implies_elim_premise_does_not_match() {
        let proof = pf! {
            [binary(atom("p"), Implies, atom("q")), atom("q")];
            (atom("q"), premise())
            (binary(atom("p"), Implies, atom("q")), premise())
            (atom("q"), implies_elim(coords![1], coords![0]))
        };
        assert_eq!(
            cite2_err!(
                proof,
                ImpliesElimPremiseDoesNotMatch,
                &coords![2],
                &coords![1],
                &coords![0]
            ),
            proof.is_justified()
        )
    }

    #[test]
    fn test_implies_elim_citation_not_implies() {
        let proof = pf! {
            [atom("p")];
            (atom("p"), premise())
            (atom("p"), implies_elim(coords![0], coords![0]))
        };
        assert_eq!(
            cite_err!(
                proof,
                ImpliesElimCitationNotImplies,
                &coords![1],
                &coords![0]
            ),
            proof.is_justified()
        )
    }
}
