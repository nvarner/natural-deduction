mod and_elim;
mod and_intro;
mod arbitrary;
mod assumption;
mod double_negation_elim;
mod false_elim;
mod iff_elim;
mod iff_intro;
mod implies_elim;
mod inplies_intro;
mod negation_elim;
mod negation_intro;
mod or_elim;
mod or_intro;
mod premise;

use crate::prelude::LawCitation::*;
use crate::prelude::Line;
use crate::proof::coords::Coords;
use crate::proof::err::Error::InvalidProposition;
use crate::proof::err::PfResult;
use crate::proof::Proof;

impl Proof {
    pub(super) fn is_line_justified(
        &self,
        line: &Line,
        line_coords: &Coords,
    ) -> PfResult {
        if !line.prop.is_valid() {
            Err(InvalidProposition {
                line: self.prettify_coords(line_coords),
            })
        } else {
            match &line.citation {
                Assumption => self.assumption(line_coords),
                Arbitrary => self.arbitrary(&line.prop, line_coords),
                Premise => self.premise(&line.prop, line_coords),
                AndElim { and } => self.and_elim(&line.prop, line_coords, and),
                AndIntro { a, b } => {
                    self.and_intro(&line.prop, line_coords, a, b)
                }
                OrElim {
                    or,
                    a_assumption,
                    b_assumption,
                } => self.or_elim(
                    &line.prop,
                    line_coords,
                    or,
                    a_assumption,
                    b_assumption,
                ),
                OrIntro { side } => {
                    self.or_intro(&line.prop, line_coords, side)
                }
                ImpliesElim {
                    implication,
                    premise,
                } => self.implies_elim(
                    &line.prop,
                    line_coords,
                    implication,
                    premise,
                ),
                ImpliesIntro { assumption } => {
                    self.implies_intro(&line.prop, line_coords, assumption)
                }
                IffElim { iff, premise } => {
                    self.iff_elim(&line.prop, line_coords, iff, premise)
                }
                IffIntro {
                    assumption_a,
                    assumption_b,
                } => self.iff_intro(
                    &line.prop,
                    line_coords,
                    assumption_a,
                    assumption_b,
                ),
                DoubleNegationElim { double_negation } => self.double_negation(
                    &line.prop,
                    line_coords,
                    double_negation,
                ),
                NegationElim { p, not_p } => {
                    self.negation_elim(&line.prop, line_coords, p, not_p)
                }
                NegationIntro { assumption } => {
                    self.negation_intro(&line.prop, line_coords, assumption)
                }
                FalseElim { f } => self.false_elim(line_coords, f),
                _ => todo!(),
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_get_structure_at() {
        let proof = pf! {
            [];
            (atom("p"), and_elim(coords![]))
            (atom("p"), and_elim(coords![]))
            {
                (atom("p"), and_elim(coords![]))
                {
                    (atom("p"), and_elim(coords![]))
                    (atom("q"), and_elim(coords![]))
                    (atom("p"), and_elim(coords![]))
                }
                (atom("p"), and_elim(coords![]))
            }
            (atom("p"), and_elim(coords![]))
        };
        let coords = coords![2, 1, 1];

        let line_guess = proof.get_structure_at(&coords);
        assert_eq!(
            Some((&line(atom("q"), and_elim(coords![]))).into()),
            line_guess
        );
    }

    #[test]
    fn test_scope_invalid() {
        let proof = pf! {
            [];
            {
                (atom("p"), assumption())
                {
                    (atom("q"), assumption())
                }
                (binary(atom("p"), And, atom("q")), and_intro(coords![0, 0], coords![0, 1, 0]))
            }
        };
        assert_eq!(
            cite_err!(
                proof,
                CitationNotInScope,
                &coords![0, 2],
                &coords![0, 1, 0]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_scope_higher_valid() {
        let proof = pf! {
            [];
            {
                (atom("p"), assumption())
                {
                    (atom("q"), assumption())
                    {
                        (binary(atom("p"), And, atom("q")), and_intro(coords![0, 0], coords![0, 1, 0]))
                    }
                    (binary(atom("p"), And, atom("p")), and_intro(coords![0, 0], coords![0, 0]))
                }
                (binary(atom("p"), And, atom("p")), and_intro(coords![0, 0], coords![0, 0]))
            }
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_scope_deeper_invalid() {
        let proof = pf! {
            [];
            {
                (atom("p"), assumption())
                {
                    (atom("q"), assumption())
                        {
                            (binary(atom("p"), And, atom("q")), and_intro(coords![0, 0], coords![0, 1, 0]))
                        }
                    (binary(atom("p"), And, atom("p")), and_intro(coords![0, 0], coords![0, 0]))
                }
                (binary(binary(atom("p"), And, atom("q")), And, atom("p")), and_intro(coords![0, 0], coords![0, 1, 1, 0]))
            }
        };
        assert_eq!(
            cite_err!(
                proof,
                CitationNotInScope,
                &coords![0, 2],
                &coords![0, 1, 1, 0]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_cite_after_invalid() {
        let proof = pf! {
            [atom("p"), atom("q")];
            (atom("p"), premise())
            (binary(atom("p"), And, atom("q")), and_intro(coords![0], coords![2]))
            (atom("q"), premise())
        };
        assert_eq!(
            cite_err!(proof, CitationNotBefore, &coords![1], &coords![2]),
            proof.is_justified()
        );
    }
}
