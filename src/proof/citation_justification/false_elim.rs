//! Valid iff cites false

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn false_elim(
        &self,
        line_coords: &Coords,
        false_coords: &Coords,
    ) -> PfResult {
        let false_prop = self.get_citation_prop(line_coords, false_coords)?;
        if let F = false_prop {
            Ok(())
        } else {
            cite_err!(self, FalseElimNotFalse, line_coords, false_coords)
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_false_elim() {
        let proof = pf! {
            [f()];
            (f(), premise())
            (binary(atom("p"), Or, atom("q")), false_elim(coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_false_elim_not_false() {
        let proof = pf! {
            [atom("p")];
            (atom("p"), premise())
            (atom("p"), false_elim(coords![0]))
        };
        assert_eq!(
            cite_err!(proof, FalseElimNotFalse, &coords![1], &coords![0]),
            proof.is_justified()
        );
    }
}
