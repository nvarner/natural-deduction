//! Valid iff matches what comes after a double negation

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn double_negation(
        &self,
        prop: &Prop,
        line_coords: &Coords,
        double_negation_coords: &Coords,
    ) -> PfResult {
        let double_negation_prop =
            self.get_citation_prop(line_coords, double_negation_coords)?;
        match double_negation_prop {
            Unary(Not, negated) => match &**negated {
                Unary(Not, doubly_negated) if **doubly_negated == *prop => {
                    Ok(())
                }
                Unary(Not, _) => cite_err!(
                    self,
                    DoubleNegationElimDoesNotMatch,
                    line_coords,
                    double_negation_coords
                ),
                _ => cite_err!(
                    self,
                    DoubleNegationElimCitationNotDoubleNegation,
                    line_coords,
                    double_negation_coords
                ),
            },
            _ => cite_err!(
                self,
                DoubleNegationElimCitationNotDoubleNegation,
                line_coords,
                double_negation_coords
            ),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_double_negation_elim() {
        let proof = pf! {
            [unary(Not, unary(Not, atom("p")))];
            (unary(Not, unary(Not, atom("p"))), premise())
            (atom("p"), double_negation_elim(coords![0]))
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_double_negation_elim_does_not_match() {
        let proof = pf! {
            [unary(Not, unary(Not, atom("p")))];
            (unary(Not, unary(Not, atom("p"))), premise())
            (atom("q"), double_negation_elim(coords![0]))
        };
        assert_eq!(
            cite_err!(
                proof,
                DoubleNegationElimDoesNotMatch,
                &coords![1],
                &coords![0]
            ),
            proof.is_justified()
        );
    }

    #[test]
    fn test_double_negation_elim_citation_not_double_negation() {
        let proof = pf! {
            [atom("p")];
            (atom("p"), premise())
            (atom("p"), double_negation_elim(coords![0]))
        };
        assert_eq!(
            cite_err!(
                proof,
                DoubleNegationElimCitationNotDoubleNegation,
                &coords![1],
                &coords![0]
            ),
            proof.is_justified()
        );
    }
}
