//! Valid iff appears as a premise of the proof

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn premise(
        &self,
        prop: &Prop,
        line_coords: &Coords,
    ) -> PfResult {
        if self.premises.contains(prop) {
            Ok(())
        } else {
            line_err!(self, PremiseNotPremise, line_coords)
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_premise() {
        let proof = pf! {
            [atom("p")];
            (atom("p"), premise())
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_premise_not_premise() {
        let proof = pf! {
            [atom("p")];
            (atom("q"), premise())
        };
        assert_eq!(
            line_err!(proof, PremiseNotPremise, &coords![0]),
            proof.is_justified()
        );
    }
}
