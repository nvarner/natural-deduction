//! Valid iff the first line in a box and is arbitrary variable

use crate::prelude::*;
use crate::proof::err::Error::*;

impl Proof {
    pub(super) fn arbitrary(
        &self,
        prop: &Prop,
        line_coords: &Coords,
    ) -> PfResult {
        match prop {
            ArbitraryVar(_) if line_coords.is_first_line_of_box() => Ok(()),
            ArbitraryVar(_) => {
                line_err!(self, ArbitraryNotFirstLineOfBox, line_coords)
            }
            _ => line_err!(self, ArbitraryNotArbitraryVar, line_coords),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;
    use crate::proof::err::Error::*;

    #[test]
    fn test_arbitrary() {
        let proof = pf! {
            [];
            {
                (arbitrary_var("c"), arbitrary())
            }
        };
        assert_eq!(Ok(()), proof.is_justified());
    }

    #[test]
    fn test_arbitrary_not_box() {
        let proof = pf! {
            [];
            (arbitrary_var("c"), arbitrary())
        };
        assert_eq!(
            line_err!(proof, ArbitraryNotFirstLineOfBox, &coords![0]),
            proof.is_justified()
        );
    }

    #[test]
    fn test_arbitrary_not_first() {
        let proof = pf! {
            [];
            {
                (arbitrary_var("a"), arbitrary())
                (arbitrary_var("b"), arbitrary())
            }
        };
        assert_eq!(
            line_err!(proof, ArbitraryNotFirstLineOfBox, &coords![0, 1]),
            proof.is_justified()
        );
    }

    #[test]
    fn test_arbitrary_not_arbitrary() {
        let proof = pf! {
            [];
            {
                (atom("p"), arbitrary())
            }
        };
        assert_eq!(
            line_err!(proof, ArbitraryNotArbitraryVar, &coords![0, 0]),
            proof.is_justified()
        );
    }
}
