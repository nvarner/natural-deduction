use crate::prelude::RefProofStructure;
use crate::proof::coords::{Coords, PrettyCoords};
use crate::proof::err::Error::*;
use crate::proof::err::{Error, PfResult};
use crate::proof::structure::ProofStructure;
use crate::prop::Prop;
use std::collections::HashSet;
use structure::assumption_box::AssumptionBox;
use structure::proof_line::Line;

#[macro_use]
pub mod err;
#[macro_use]
pub mod coords;
#[macro_use]
pub mod structure;
#[macro_use]
pub mod convenience;

pub mod citation;
mod citation_justification;

#[derive(Debug, Clone)]
pub struct Proof {
    premises: HashSet<Prop>,
    body: Vec<ProofStructure>,
}

impl Proof {
    pub fn new(premises: HashSet<Prop>, body: Vec<ProofStructure>) -> Self {
        Self { premises, body }
    }

    pub fn is_justified(&self) -> PfResult {
        let err = self
            .structure_iter()
            .map(|(coords, structure)| {
                self.is_structure_justified(structure.into(), &coords)
            })
            .find_map(|result| result.err());
        match err {
            None => Ok(()),
            Some(err) => Err(err),
        }
    }

    fn structure_iter(
        &self,
    ) -> impl Iterator<Item = (Coords, &ProofStructure)> {
        self.body
            .iter()
            .enumerate()
            .map(|(i, structure)| (Coords(vec![i]), structure))
    }

    fn is_structure_justified(
        &self,
        structure: RefProofStructure,
        structure_coords: &Coords,
    ) -> PfResult {
        match structure {
            RefProofStructure::Line(line) => {
                self.is_line_justified(line, structure_coords)
            }
            RefProofStructure::Box(assumption_box) => {
                self.is_box_justified(assumption_box, structure_coords)
            }
        }
    }

    fn get_citation_prop(
        &self,
        line_coords: &Coords,
        citation_coords: &Coords,
    ) -> Result<&Prop, Error> {
        match self.get_structure_at(citation_coords) {
            None => Err(CitesMissingLine {
                line: self.prettify_coords(line_coords),
                citation_line: citation_coords.clone(),
            }),
            Some(_) if !citation_coords.is_before(line_coords) => {
                cite_err!(self, CitationNotBefore, line_coords, citation_coords)
            }
            Some(_) if !line_coords.can_cite(citation_coords) => {
                cite_err!(
                    self,
                    CitationNotInScope,
                    line_coords,
                    citation_coords
                )
            }
            Some(RefProofStructure::Box(_)) => {
                cite_err!(self, CitesBox, line_coords, citation_coords)
            }
            Some(RefProofStructure::Line(Line { prop, .. })) => Ok(prop),
        }
    }

    fn get_citation_box(
        &self,
        line_coords: &Coords,
        citation_coords: &Coords,
    ) -> Result<&AssumptionBox, Error> {
        match self.get_structure_at(citation_coords) {
            None => Err(CitesMissingLine {
                line: self.prettify_coords(line_coords),
                citation_line: citation_coords.clone(),
            }),
            Some(_) if !citation_coords.is_before(line_coords) => {
                cite_err!(self, CitationNotBefore, line_coords, citation_coords)
            }
            Some(RefProofStructure::Line(_)) => {
                cite_err!(self, CitesLine, line_coords, citation_coords)
            }
            Some(RefProofStructure::Box(assumption_box)) => Ok(assumption_box),
        }
    }

    fn is_box_justified(
        &self,
        assumption_box: &AssumptionBox,
        box_coords: &Coords,
    ) -> PfResult {
        let err = assumption_box
            .iter_coords(box_coords)
            .map(|(coords, structure)| {
                self.is_structure_justified(structure, &coords)
            })
            .find_map(|result| result.err());
        match err {
            None => Ok(()),
            Some(err) => Err(err),
        }
    }

    fn prettify_coords(&self, coords: &Coords) -> PrettyCoords {
        PrettyCoords(coords.clone())
    }

    fn get_structure_at(&self, coords: &Coords) -> Option<RefProofStructure> {
        let (index, rest) = coords.0.split_first()?;
        let substructure = self.body.get(*index)?;
        substructure.get_at(rest)
    }
}
