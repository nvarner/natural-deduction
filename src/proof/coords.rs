use std::cmp::Ordering;
use std::fmt;
use std::fmt::Display;

#[macro_export]
macro_rules! coords {
    [$($x:expr),* $(,)?] => ($crate::proof::Coords(vec![$($x),*]))
}

#[derive(Eq, PartialEq, Debug, Clone)]
pub struct Coords(pub Vec<usize>);

impl Coords {
    pub fn is_before(&self, other: &Coords) -> bool {
        let self_iter = self.0.iter();
        let other_iter = other.0.iter();
        let diverged_early =
            self_iter.zip(other_iter).find_map(|(l, r)| match l.cmp(r) {
                Ordering::Less => Some(true),
                Ordering::Greater => Some(false),
                Ordering::Equal => None,
            });
        match diverged_early {
            Some(before) => before,
            // boxes end after everything inside them
            None => self.0.len() > other.0.len(),
        }
    }

    pub fn can_cite(&self, citation: &Coords) -> bool {
        let self_iter = self.0.iter();
        let citation_iter = citation.0.iter();
        let divergence_point = self_iter
            .zip(citation_iter)
            .take_while(|(a, b)| a == b)
            .count();
        divergence_point + 1 == citation.0.len()
    }

    pub fn is_first_line_of_box(&self) -> bool {
        let box_index = self.0.last();
        match box_index {
            Some(0) => self.0.len() > 1, // not a box if it's first line of the proof
            _ => false,
        }
    }

    pub fn with_added_index(&self, index: usize) -> Self {
        let mut new = self.clone();
        new.0.push(index);
        new
    }
}

// TODO: use line numbers instead
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct PrettyCoords(pub Coords);

impl Display for PrettyCoords {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.0)
    }
}
