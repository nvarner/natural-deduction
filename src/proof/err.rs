use crate::proof::coords::Coords;
use crate::proof::coords::PrettyCoords;
use thiserror::Error;

macro_rules! line_err {
    ($proof:ident, $err:ident, $coords:expr) => {
        Err($err {
            line: $proof.prettify_coords($coords),
        })
    };
}

macro_rules! cite_err {
    ($proof:ident, $err:ident, $coords:expr, $citation_coords: expr) => {
        Err($err {
            line: $proof.prettify_coords($coords),
            citation_line: $proof.prettify_coords($citation_coords),
        })
    };
}

macro_rules! cite2_err {
    ($proof:ident, $err:ident, $coords:expr, $a_coords: expr, $b_coords: expr) => {
        Err($err {
            line: $proof.prettify_coords($coords),
            citation_a_line: $proof.prettify_coords($a_coords),
            citation_b_line: $proof.prettify_coords($b_coords),
        })
    };
}

macro_rules! cite3_err {
    ($proof:ident, $err:ident, $coords:expr, $a_coords: expr, $b_coords: expr, $c_coords:expr) => {
        Err($err {
            line: $proof.prettify_coords($coords),
            citation_a_line: $proof.prettify_coords($a_coords),
            citation_b_line: $proof.prettify_coords($b_coords),
            citation_c_line: $proof.prettify_coords($c_coords),
        })
    };
}

pub type PfResult = Result<(), Error>;

#[derive(Error, Debug, Eq, PartialEq)]
pub enum Error {
    #[error("line {line} cites {citation_line:?}, which does not exist")]
    CitesMissingLine {
        line: PrettyCoords,
        citation_line: Coords,
    },
    #[error("line {line} cites box {citation_line}, but expected a line")]
    CitesBox {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites line {citation_line}, but expected a box")]
    CitesLine {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites {citation_line}, which does not come before")]
    CitationNotBefore {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites {citation_line}, which is not in scope")]
    CitationNotInScope {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} has an invalid proposition")]
    InvalidProposition { line: PrettyCoords },
    #[error("line {line} is an assumption but not made at the top of a box")]
    AssumptionNotFirstLineOfBox { line: PrettyCoords },
    #[error("line {line} is arbitrary but not at the top of a box")]
    ArbitraryNotFirstLineOfBox { line: PrettyCoords },
    #[error("line {line} is not arbitrary but ist justified as arbitrary")]
    ArbitraryNotArbitraryVar { line: PrettyCoords },
    #[error("line {line} is not a premise but is justified as one")]
    PremiseNotPremise { line: PrettyCoords },
    #[error("line {line} cites and-elim on line {citation_line}, but doesn't match either side")]
    AndElimDoesNotMatchEitherSide {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites and-elim on line {citation_line}, which isn't an and")]
    AndElimCitationNotAnd {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites and-intro with lines {citation_a_line} and {citation_b_line}, but neither matches either side")]
    AndIntroNotMatchAny {
        line: PrettyCoords,
        citation_a_line: PrettyCoords,
        citation_b_line: PrettyCoords,
    },
    #[error("line {line} cites and-intro with lines {citation_a_line} and {citation_b_line}, but neither matches the right side")]
    AndIntroNotMatchRight {
        line: PrettyCoords,
        citation_a_line: PrettyCoords,
        citation_b_line: PrettyCoords,
    },
    #[error("line {line} cites and-intro with lines {citation_a_line} and {citation_b_line}, but neither matches the left side")]
    AndIntroNotMatchLeft {
        line: PrettyCoords,
        citation_a_line: PrettyCoords,
        citation_b_line: PrettyCoords,
    },
    #[error("line {line} cites and-intro but is not an and")]
    AndIntroNotAnd { line: PrettyCoords },
    #[error("line {line} cites or-intro on line {citation_line}, but it isn't an or")]
    OrElimCitationNotOr {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites or-elim on {citation_a_line} with assumptions on {citation_b_line} and {citation_c_line}, but the initial propositions of the assumption boxes do not match the two sides of the or statement")]
    OrElimNotMatch {
        line: PrettyCoords,
        citation_a_line: PrettyCoords,
        citation_b_line: PrettyCoords,
        citation_c_line: PrettyCoords,
    },
    #[error("line {line} cites or-elim with assumption {citation_line}, but the conclusion does not match the last line of the box")]
    OrElimConclusionNotMatch {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites or-intro with line {citation_line}, but that line does not match the left or right side of the or statement")]
    OrIntroDoesNotMatchEitherSide {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites or-intro but is not an or")]
    OrIntroNotOr { line: PrettyCoords },
    #[error("line {line} cites implies-elim on the implication on line {citation_line} but it does not match the conclusion")]
    ImpliesElimConclusionDoesNotMatch {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites implies-elim on the implication {citation_a_line} with the premise {citation_b_line}, but the premises do not match")]
    ImpliesElimPremiseDoesNotMatch {
        line: PrettyCoords,
        citation_a_line: PrettyCoords,
        citation_b_line: PrettyCoords,
    },
    #[error("line {line} cites implies-elim, but line {citation_line} isn't an implication")]
    ImpliesElimCitationNotImplies {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites implies-intro on line {citation_line}, but its first line does mot match the premise")]
    ImpliesIntroPremiseDoesNotMatch {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites implies-intro on lines {citation_line}, but its last line does not match the conclusion")]
    ImpliesIntroConclusionDoesNotMatch {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites implies-intro but is not an implication")]
    ImpliesIntroNotImplies { line: PrettyCoords },
    #[error("line {line} cites double-negation-elim on line {citation_line} but does not match what is being doubly negated")]
    DoubleNegationElimDoesNotMatch {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites double-negation-elim on line {citation_line}, but that isn't a double negation")]
    DoubleNegationElimCitationNotDoubleNegation {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites iff-elim on the implication on line {citation_line} but it does not match the conclusion")]
    IffElimConclusionDoesNotMatch {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites iff-elim on the implication {citation_a_line} with the premise {citation_b_line}, but the premises do not match")]
    IffElimPremiseDoesNotMatch {
        line: PrettyCoords,
        citation_a_line: PrettyCoords,
        citation_b_line: PrettyCoords,
    },
    #[error(
        "line {line} cites iff-elim, but line {citation_line} isn't an iff"
    )]
    IffElimCitationNotIff {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites iff-intro on lines {citation_a_line} and {citation_b_line}, but the lines do not match up")]
    IffIntroDoesNotMatch {
        line: PrettyCoords,
        citation_a_line: PrettyCoords,
        citation_b_line: PrettyCoords,
    },
    #[error("line {line} cites iff-intro but is not an iff")]
    IffIntroNotIff { line: PrettyCoords },
    #[error("line {line} cites negation-elim on lines {citation_a_line} and {citation_b_line}, but one isn't the negation of the other")]
    NegationElimNotNegation {
        line: PrettyCoords,
        citation_a_line: PrettyCoords,
        citation_b_line: PrettyCoords,
    },
    #[error("line {line} cites negation-elim but is not false")]
    NegationElimNotFalse { line: PrettyCoords },
    #[error("line {line} cites negation-intro on lines {citation_line}, but it does not end with false")]
    NegationIntroNoFalse {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites negation-intro on lines {citation_line}, but it is not the negation of the first line")]
    NegationIntroAssumptionDoesNotMatch {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
    #[error("line {line} cites negation-intro but is not a negation")]
    NegationIntroNotNegation { line: PrettyCoords },
    #[error("line {line} cites false-elim on line {citation_line}, but it isn't false")]
    FalseElimNotFalse {
        line: PrettyCoords,
        citation_line: PrettyCoords,
    },
}
