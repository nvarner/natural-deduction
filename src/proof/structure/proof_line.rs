use crate::proof::citation::LawCitation;
use crate::prop::Prop;

#[derive(Eq, PartialEq, Debug, Clone)]
pub struct Line {
    pub prop: Prop,
    pub citation: LawCitation,
}
