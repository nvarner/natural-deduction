//! A proof structure is a fundamental component of a proof. In particular,
//! lines and boxes are proof structures. Boxes contain further proof
//! structures, possibly including other boxes.

#[macro_use]
pub mod convenience;
pub mod assumption_box;
pub mod proof_line;

use assumption_box::AssumptionBox;
use proof_line::Line;

#[derive(Eq, PartialEq, Debug, Copy, Clone)]
pub enum RefProofStructure<'a> {
    Line(&'a Line),
    Box(&'a AssumptionBox),
}

impl<'a> From<&'a ProofStructure> for RefProofStructure<'a> {
    fn from(structure: &'a ProofStructure) -> Self {
        match structure {
            ProofStructure::Line(line) => Self::Line(line),
            ProofStructure::Box(assumption_box) => Self::Box(assumption_box),
        }
    }
}

impl<'a> RefProofStructure<'a> {
    pub fn get_at(&self, coords: &[usize]) -> Option<Self> {
        match coords.split_first() {
            None => Some(*self),
            Some((index, rest)) => {
                let substructure = self.contents()?.get(*index)?;
                substructure.get_at(rest)
            }
        }
    }

    fn contents<'b>(&'b self) -> Option<&'a AssumptionBox> {
        match self {
            Self::Line(_) => None,
            Self::Box(inner) => Some(inner),
        }
    }
}

#[derive(Eq, PartialEq, Debug, Clone)]
pub enum ProofStructure {
    Line(Line),
    Box(AssumptionBox),
}

impl ProofStructure {
    pub fn get_at(&self, coords: &[usize]) -> Option<RefProofStructure> {
        match coords.split_first() {
            None => Some(self.into()),
            Some((index, rest)) => {
                let substructure = self.contents()?.get(*index)?;
                substructure.get_at(rest)
            }
        }
    }

    fn contents(&self) -> Option<&AssumptionBox> {
        match self {
            Self::Line(_) => None,
            Self::Box(inner) => Some(inner),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;

    #[test]
    fn test_get_at() {
        let structure = pf_box![
            (atom("p"), and_elim(coords![]))
            {
                (atom("p"), and_elim(coords![]))
                (atom("p"), and_elim(coords![]))
                {
                    (atom("q"), and_elim(coords![]))
                    (atom("p"), and_elim(coords![]))
                    (atom("p"), and_elim(coords![]))
                }
                (atom("p"), and_elim(coords![]))
            }
            (atom("p"), and_elim(coords![]))
        ];

        let line_guess = structure.get_at(&[1, 2, 0]);
        assert_eq!(
            Some((&line(atom("q"), and_elim(coords![]))).into()),
            line_guess
        );
    }
}
