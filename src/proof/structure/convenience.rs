//! Convenient functions/macros to quickly construct proof structures

use crate::prelude::*;

pub const fn line(prop: Prop, citation: LawCitation) -> ProofStructure {
    ProofStructure::Line(Line { prop, citation })
}

macro_rules! pf_box_helper {
    [{ ($prop_1:expr, $citation_1:expr) }; { $($middle:tt)* }; ($prop_2:expr, $citation_2:expr)] => ({
        $crate::proof::structure::ProofStructure::Box(
            $crate::proof::structure::assumption_box::AssumptionBox::new_non_unit(
                $crate::proof::structure::proof_line::Line {
                    prop: $prop_1,
                    citation: $citation_1,
                },
                vec![$($middle),*],
                $crate::proof::structure::proof_line::Line {
                    prop: $prop_2,
                    citation: $citation_2,
                }
            )
        )
    });
    [{ ($prop:expr, $citation:expr) }; { $($middle:tt)* }; ($prop_next:expr, $citation_next:expr) $($rest:tt)*] => (
        pf_box_helper![
            { ($prop, $citation) };
            {
                $($middle)*
                { $crate::proof::structure::convenience::line($prop_next, $citation_next) }
            };
            $($rest)*
        ]
    );
    [{ ($prop:expr, $citation:expr) }; { $($middle:tt)* }; { $($next:tt)* } $($rest:tt)*] => (
        pf_box_helper![
            { ($prop, $citation) };
            {
                $($middle)*
                { pf_box![$($next)*] }
            };
            $($rest)*
        ]
    );
}

macro_rules! pf_box {
    [($prop:expr, $citation:expr)] => ({
        $crate::proof::structure::ProofStructure::Box(
            $crate::proof::structure::assumption_box::AssumptionBox::new_unit(
                $crate::proof::structure::proof_line::Line {
                    prop: $prop,
                    citation: $citation,
                }
            )
        )
    });
    [($prop:expr, $citation:expr) $($rest:tt)*] => (
        pf_box_helper![{ ($prop, $citation) }; {}; $($rest)*]
    );
}

#[cfg(test)]
mod test {
    use crate::prelude::{
        assumption, line, AssumptionBox, Line, ProofStructure,
    };
    use crate::prop::atom;

    #[test]
    fn test_pf_structure_unit_line() {
        let actual = pf_box![(atom("p"), assumption())];
        assert_eq!(
            ProofStructure::Box(AssumptionBox::Unit {
                line: Line {
                    prop: atom("p"),
                    citation: assumption(),
                },
            }),
            actual
        );
    }

    #[test]
    fn test_pf_structure_non_unit_lines() {
        let actual = pf_box![(atom("p"), assumption())(
            atom("q"),
            assumption()
        )(atom("r"), assumption())];
        assert_eq!(
            ProofStructure::Box(AssumptionBox::NonUnit {
                first: Line {
                    prop: atom("p"),
                    citation: assumption(),
                },
                middle: vec![line(atom("q"), assumption())],
                last: Line {
                    prop: atom("r"),
                    citation: assumption(),
                }
            }),
            actual
        );
    }

    #[test]
    fn test_pf_structure_recursive() {
        let actual = pf_box![
            (atom("p"), assumption())
            {
                (atom("r"), assumption())
            }
            (atom("q"), assumption())
        ];
        assert_eq!(
            ProofStructure::Box(AssumptionBox::NonUnit {
                first: Line {
                    prop: atom("p"),
                    citation: assumption(),
                },
                middle: vec![ProofStructure::Box(AssumptionBox::Unit {
                    line: Line {
                        prop: atom("r"),
                        citation: assumption(),
                    },
                })],
                last: Line {
                    prop: atom("q"),
                    citation: assumption(),
                }
            }),
            actual
        );
    }
}
