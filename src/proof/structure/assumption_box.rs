use crate::prelude::Line;
use crate::proof::coords::Coords;
use crate::proof::structure::{ProofStructure, RefProofStructure};
use std::iter;

#[derive(Eq, PartialEq, Debug, Clone)]
pub enum AssumptionBox {
    Unit {
        line: Line,
    },
    NonUnit {
        first: Line,
        middle: Vec<ProofStructure>,
        last: Line,
    },
}

impl AssumptionBox {
    pub fn new_unit(line: Line) -> Self {
        Self::Unit { line }
    }

    pub fn new_non_unit(
        first: Line,
        middle: Vec<ProofStructure>,
        last: Line,
    ) -> Self {
        Self::NonUnit {
            first,
            middle,
            last,
        }
    }

    pub fn first(&self) -> &Line {
        match self {
            Self::Unit { line } => line,
            Self::NonUnit { first, .. } => first,
        }
    }

    pub fn last(&self) -> &Line {
        match self {
            Self::Unit { line } => line,
            Self::NonUnit { last, .. } => last,
        }
    }

    pub fn get(&self, index: usize) -> Option<RefProofStructure> {
        match self {
            Self::Unit { line } if index == 0 => {
                Some(RefProofStructure::Line(line))
            }
            Self::NonUnit { first, .. } if index == 0 => {
                Some(RefProofStructure::Line(first))
            }
            Self::NonUnit { middle, .. } if index < middle.len() + 1 => {
                Some((&middle[index - 1]).into())
            }
            Self::NonUnit { middle, last, .. } if index == middle.len() + 1 => {
                Some(RefProofStructure::Line(last))
            }
            _ => None,
        }
    }

    pub fn iter(&self) -> Box<dyn Iterator<Item = RefProofStructure> + '_> {
        match self {
            Self::Unit { line } => {
                Box::new(iter::once(RefProofStructure::Line(line)))
            }
            Self::NonUnit {
                first,
                middle,
                last,
            } => Box::new(
                iter::once(RefProofStructure::Line(first))
                    .chain(middle.iter().map(|x| x.into()))
                    .chain(iter::once(RefProofStructure::Line(last))),
            ),
        }
    }

    pub fn iter_coords<'a>(
        &'a self,
        coords: &'a Coords,
    ) -> impl Iterator<Item = (Coords, RefProofStructure<'a>)> + 'a {
        self.iter()
            .enumerate()
            .map(|(i, structure)| (coords.with_added_index(i), structure))
    }
}
