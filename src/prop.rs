use std::collections::HashSet;
use std::hash::Hash;

#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub struct AtomName(String);

#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub struct QuantVarName(String);

#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub struct ArbitraryVarName(String);

#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub struct ExistsVarName(String);

#[derive(Eq, PartialEq, Debug, Copy, Clone, Hash)]
pub enum UnaryOp {
    Not,
}

#[derive(Eq, PartialEq, Debug, Copy, Clone, Hash)]
pub enum BinaryOp {
    And,
    Or,
    Implies,
    Iff,
}

#[derive(Eq, PartialEq, Debug, Copy, Clone, Hash)]
pub enum Quant {
    ForAll,
    Exists,
}

#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub enum Prop {
    /// True
    T,
    /// False
    F,
    /// Some proposition, like p or q, which could evaluate to T or F
    Atom(AtomName),
    /// A variable which is quantified, eg. x in \forall x P(x)
    QuantVar(QuantVarName),
    /// A variable which is declared arbitrary at the start of an assumption box
    ArbitraryVar(ArbitraryVarName),
    /// A variable which is defined by an exists statement, eg. x_0 in \exists x P(x); P(x_0)
    ExistsVar(ExistsVarName),
    /// A unary operator applied to a proposition
    Unary(UnaryOp, Box<Prop>),
    /// A binary operator applied to two propositions
    Binary(Box<Prop>, BinaryOp, Box<Prop>),
    /// A quantified statement, eg. \forall x P(x)
    Quant(Quant, QuantVarName, Box<Prop>),
}

impl Prop {
    pub fn sub(&self, p: &Self, q: &Self) -> Self {
        if self == p {
            q.clone()
        } else {
            match self {
                Self::Unary(op, a) => Self::Unary(*op, Box::new(a.sub(p, q))),
                Self::Binary(a, op, b) => Self::Binary(
                    Box::new(a.sub(p, q)),
                    *op,
                    Box::new(b.sub(p, q)),
                ),
                Self::Quant(quant, var, a) => {
                    Self::Quant(*quant, var.clone(), Box::new(a.sub(p, q)))
                }
                _ => self.clone(),
            }
        }
    }

    pub fn is_valid(&self) -> bool {
        self.vars_need_quant().is_empty()
    }

    /// The set of quantified variables which are not actually quantified
    fn vars_need_quant(&self) -> HashSet<&QuantVarName> {
        match self {
            Self::QuantVar(name) => {
                let mut set = HashSet::new();
                set.insert(name);
                set
            }
            Self::Unary(_, a) => a.vars_need_quant(),
            Self::Binary(a, _, b) => {
                let mut vars = a.vars_need_quant();
                vars.extend(b.vars_need_quant());
                vars
            }
            Self::Quant(_, var, a) => {
                let mut vars = a.vars_need_quant();
                vars.remove(var);
                vars
            }
            _ => HashSet::new(),
        }
    }
}

pub fn t() -> Prop {
    Prop::T
}

pub fn f() -> Prop {
    Prop::F
}

pub fn atom(name: &str) -> Prop {
    Prop::Atom(AtomName(name.to_owned()))
}

pub fn quant_var(name: &str) -> Prop {
    Prop::QuantVar(QuantVarName(name.to_owned()))
}

pub fn arbitrary_var(name: &str) -> Prop {
    Prop::ArbitraryVar(ArbitraryVarName(name.to_owned()))
}

pub fn exists_var(name: &str) -> Prop {
    Prop::ExistsVar(ExistsVarName(name.to_owned()))
}

pub fn unary(op: UnaryOp, prop: Prop) -> Prop {
    Prop::Unary(op, Box::new(prop))
}

pub fn binary(prop_a: Prop, op: BinaryOp, prop_b: Prop) -> Prop {
    Prop::Binary(Box::new(prop_a), op, Box::new(prop_b))
}

pub fn quant(quant: Quant, var_name: &str, prop: Prop) -> Prop {
    Prop::Quant(quant, QuantVarName(var_name.to_owned()), Box::new(prop))
}

#[cfg(test)]
mod test {
    use super::BinaryOp::*;
    use super::Quant::*;
    use super::UnaryOp::*;
    use super::*;

    #[test]
    fn test_eq() {
        let p = quant(
            ForAll,
            "x",
            binary(atom("p"), And, unary(Not, quant_var("x"))),
        );
        let q = quant(
            ForAll,
            "x",
            binary(atom("p"), And, unary(Not, quant_var("x"))),
        );
        assert_eq!(p, q);
    }

    #[test]
    fn test_ne() {
        let p = quant(
            ForAll,
            "x",
            binary(atom("p"), And, unary(Not, quant_var("x"))),
        );
        let q = quant(
            Exists,
            "x",
            binary(quant_var("x"), And, unary(Not, atom("p"))),
        );
        assert_ne!(p, q);
    }

    #[test]
    fn test_sub() {
        let p = quant(
            ForAll,
            "x",
            binary(atom("p"), And, unary(Not, quant_var("x"))),
        );
        let q = p.sub(&atom("p"), &binary(atom("q"), Or, atom("r")));
        let r = quant(
            ForAll,
            "x",
            binary(
                binary(atom("q"), Or, atom("r")),
                And,
                unary(Not, quant_var("x")),
            ),
        );
        assert_eq!(q, r);
    }

    #[test]
    fn test_sub_many() {
        let p = binary(atom("p"), Implies, atom("p"));
        let q = p.sub(&atom("p"), &atom("q"));
        let r = binary(atom("q"), Implies, atom("q"));
        assert_eq!(q, r);
    }

    #[test]
    fn test_sub_none() {
        let p = binary(atom("p"), Implies, atom("p"));
        let q = p.sub(&atom("z"), &atom("x"));
        let r = binary(atom("p"), Implies, atom("p"));
        assert_eq!(q, r);
    }

    #[test]
    fn test_valid() {
        let p = quant(
            ForAll,
            "x",
            binary(atom("p"), And, unary(Not, quant_var("x"))),
        );
        assert!(p.is_valid());
    }

    #[test]
    fn test_invalid() {
        let p = quant(
            ForAll,
            "x",
            binary(atom("p"), And, unary(Not, quant_var("y"))),
        );
        assert!(!p.is_valid());
    }
}
